import type { IUserSchema, IUser } from './model/types/userShema'
import { userReducer } from './model/slice/userSlice'
import { getUserAuthData } from './model/selectors/getUserAuthData/getUserAuthData'

export { IUserSchema, IUser, userReducer, getUserAuthData }
