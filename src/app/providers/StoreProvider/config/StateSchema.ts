import { AnyAction, CombinedState, EnhancedStore, Reducer, ReducersMapObject } from '@reduxjs/toolkit';
import { ICounterSchema } from 'entities/Counter';
import { IUserSchema } from 'entities/User';
import { ILoginSchema } from 'features/AuthByUserName';

export interface IStateSchema {
    counter: ICounterSchema,
    user: IUserSchema,

    // Асинхронные редюсеры
    loginForm?: ILoginSchema
}

export type IStateSchemaKey = keyof IStateSchema

export interface ReducerManager {
    getReducerMap: () => ReducersMapObject<IStateSchema>,
    reduce: (state: IStateSchema, action: AnyAction) => CombinedState<IStateSchema>
    add: (key: IStateSchemaKey, reducer: Reducer) => void
    remove: (key: IStateSchemaKey) => void
}

export interface ReduxStoreWithManager extends EnhancedStore<IStateSchema> {
    reducerManager: ReducerManager
}