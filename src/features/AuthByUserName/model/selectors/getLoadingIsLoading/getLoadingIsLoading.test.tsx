import { DeepPartial } from '@reduxjs/toolkit'
import { IStateSchema } from 'app/providers/StoreProvider'
import { getLoadingIsLoading } from './getLoadingIsLoading'

describe('getLoadingIsLoading.test', () => {
  test('should return true', () => {
    const state: DeepPartial<IStateSchema> = {
      loginForm: {
        isLoading: true,
      },
    }
    expect(getLoadingIsLoading(state as IStateSchema)).toEqual(true)
  })

  test('should return false', () => {
    const state: DeepPartial<IStateSchema> = {
      loginForm: {
      },
    }
    expect(getLoadingIsLoading(state as IStateSchema)).toEqual(false)
  })
})

