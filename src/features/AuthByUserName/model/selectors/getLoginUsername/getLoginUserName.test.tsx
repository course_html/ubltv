import { DeepPartial } from '@reduxjs/toolkit'
import { IStateSchema } from 'app/providers/StoreProvider'
import { getLoginUsername } from './getLoginUsername'

describe('getLoadingIsLoading.test', () => {
  test('should return true', () => {
    const state: DeepPartial<IStateSchema> = {
      loginForm: {
        username: 'name',
      },
    }
    expect(getLoginUsername(state as IStateSchema)).toEqual('name ')
  })

  test('sshould work with empty state', () => {
    const state: DeepPartial<IStateSchema> = {
      loginForm: {
      },
    }
    expect(getLoginUsername(state as IStateSchema)).toEqual('')
  })
})

