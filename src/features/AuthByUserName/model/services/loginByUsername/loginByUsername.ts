import { createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'
import { IUser } from 'entities/User'
import { userActions } from 'entities/User/model/slice/userSlice'
import { USER_LOCALSTORAGE_KEY } from 'shared/const/localstorage'

enum LoginErrors {
  
}

interface IloginByUsernameProps {
  username: string
  password: string
}

export const loginByUsername = createAsyncThunk<IUser, IloginByUsernameProps, { rejectValue: string }>(
  'login/loginByUsername',
  async (authData, thunkApi) => {
    try {
      const responce = await axios.post<IUser>(
        'http://localhost:8000/login',
        authData
      )

      if (!responce.data) {
        throw new Error()
      }

      localStorage.setItem(USER_LOCALSTORAGE_KEY, JSON.stringify(responce.data))
      thunkApi.dispatch(userActions.setAuthData(responce.data))

      return responce.data
    } catch (e) {
      console.log(e)
      return thunkApi.rejectWithValue('error')
    }
  }
)


