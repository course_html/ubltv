import { DeepPartial } from "@reduxjs/toolkit";
import { ILoginSchema } from "../types/loginSchema";
import { loginActions, loginReducer } from "./loginSlice";

describe('loginSlice.test', () => {
    test('test set username', () => {
        const state: DeepPartial<ILoginSchema> = {username: 'name'}
        expect(loginReducer(state as ILoginSchema, loginActions.setUsername('name'))).toEqual({username: "name"});
    })
    test('test set password', () => {
        const state: DeepPartial<ILoginSchema> = {password: '123'}
        expect(loginReducer(state as ILoginSchema, loginActions.setPassword('123'))).toEqual({password: "123"});
    });
});