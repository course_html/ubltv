/* eslint-disable i18next/no-literal-string */
import { Reducer } from '@reduxjs/toolkit'
import { IStateSchemaKey } from 'app/providers/StoreProvider/config/StateSchema'
import { getLoadingIsLoading } from 'features/AuthByUserName/model/selectors/getLoadingIsLoading/getLoadingIsLoading'
import { getLoginError } from 'features/AuthByUserName/model/selectors/getLoginError/getLoginError'
import { getLoginPassword } from 'features/AuthByUserName/model/selectors/getLoginPassword/getLoginPassword'
import { getLoginUsername } from 'features/AuthByUserName/model/selectors/getLoginUsername/getLoginUsername'
import { loginByUsername } from 'features/AuthByUserName/model/services/loginByUsername/loginByUsername'
import {
  loginActions,
  loginReducer,
} from 'features/AuthByUserName/model/slice/loginSlice'
import { memo, useCallback } from 'react'
import { useTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux'
import i18n from 'shared/config/i18n/i18n'
import { classNames } from 'shared/lib/classNames/classNames'
import { DinamicModelLoader, ReducersList } from 'shared/lib/components/DinamicModelLoader/DinamicModelLoader'
import { Button, ButtonTheme } from 'shared/ui/Button/Button'
import { Input } from 'shared/ui/Input/Input'
import { Text, TextThemeEnum } from 'shared/ui/Text/Text'
import cls from './LoginForm.module.scss'

export interface ILoginFormProps {
  className?: string
}

const initialReducers: ReducersList = {
  loginForm: loginReducer
}

const LoginForm = memo(({ className }: ILoginFormProps) => {
  const { t } = useTranslation()
  const dispatch = useDispatch()

  // const { username, password, isLoading, error } = useSelector(getLoginState)
  const username = useSelector(getLoginUsername)
  const password = useSelector(getLoginPassword)
  const isLoading = useSelector(getLoadingIsLoading)
  const error = useSelector(getLoginError)

  const onChangeUsername = useCallback(
    (name: string) => {
      dispatch(loginActions.setUsername(name))
    },
    [dispatch]
  )

  const onChangePassword = useCallback(
    (password: string) => {
      dispatch(loginActions.setPassword(password))
    },
    [dispatch]
  )

  const onLoginClick = useCallback(() => {
    dispatch(
      loginByUsername({
        username,
        password,
      })
    )
  }, [dispatch, username, password])

  return (
    <DinamicModelLoader removeAfterUnmount reducers={initialReducers}>
      <div className={classNames(cls.LoginForm, {}, [className])}>
        <Text title={t('Форма авторизации')} />
        {error && (
          <Text
            text={i18n.t('Вы ввели неверный логин или пароль')}
            theme={TextThemeEnum.ERROR}
          />
        )}
        <Input
          className={cls.input}
          type="text"
          placeholder={t('Введите Имя')}
          autoFocus
          onChange={onChangeUsername}
          value={username}
        />
        <Input
          className={cls.input}
          type="text"
          placeholder={t('Введите пароль')}
          onChange={onChangePassword}
          value={password}
        />
        <Button
          theme={ButtonTheme.BACKGROUND}
          onClick={onLoginClick}
          className={classNames(cls.loginBtn)}
          disabled={isLoading as boolean}
        >
          {t('Войти')}
        </Button>
      </div>
    </DinamicModelLoader>
  )
})

export default LoginForm
