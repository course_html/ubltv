import { classNames } from 'shared/lib/classNames/classNames'
import { FC } from 'react'
import cls from './Text.module.scss'

export enum TextThemeEnum {
  PRIMARY = 'primary',
  ERROR = 'error'
}

interface ITextPropsProps {
  className?: string
  title?: string
  text?: string
  theme?: TextThemeEnum
}

export const Text: FC<ITextPropsProps> = (props) => {
  const { className, title, text, theme = TextThemeEnum.PRIMARY } = props

  return (
    <div className={classNames(cls.Text, {[cls[theme]]: true}, [className])}>
      {title && <h2>{title}</h2>}
      {text && <h2>{title}</h2>}
    </div>
  )
}
