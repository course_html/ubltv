import { Reducer } from '@reduxjs/toolkit'
import { ReduxStoreWithManager } from 'app/providers/StoreProvider'
import { IStateSchemaKey } from 'app/providers/StoreProvider/config/StateSchema'
import { ReactNode, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { useStore } from 'react-redux'

export type ReducersList = {
  [name in IStateSchemaKey]?: Reducer
}

type ReducerListEntry = [IStateSchemaKey, Reducer]

interface IDinamicModelLoaderProps {
  reducers: ReducersList
  children: ReactNode
  removeAfterUnmount?: boolean
}

export const DinamicModelLoader = ({
  reducers,
  children,
  removeAfterUnmount,
}: IDinamicModelLoaderProps) => {
  const { t } = useTranslation()
  const store = useStore() as ReduxStoreWithManager

  useEffect(() => {
    Object.entries(reducers).forEach(([name, reducer]: ReducerListEntry) => {
      store.reducerManager.add(name, reducer)
    })

    return () => {
      if (removeAfterUnmount) {
        Object.entries(reducers).forEach(
          ([name]: ReducerListEntry) => {
            store.reducerManager.remove(name)
          }
        )
      }
    }
    // eslint-disable-next-line
  }, [])

  return <div>{children}</div>
}
